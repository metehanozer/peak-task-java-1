package util;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class PageUtil {

    private static final int TIMEOUT = 15;
    private static final int POLLING = 100;

    public WebDriver webDriver;
    private WebDriverWait webDriverWait;

    public PageUtil(WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, TIMEOUT, POLLING);
        PageFactory.initElements(new AjaxElementLocatorFactory(webDriver, TIMEOUT), this);
    }

    public void refreshPage() {
        webDriver.navigate().refresh();
    }

    public String getTextTo(By by) {
        return waitForFindVisible(by).getText();
    }

    public String getAttributeTo(By by, String attribute) {
        return waitForFindVisible(by).getAttribute(attribute);
    }

    public void sendKeysTo(By by, String string) {
        waitForFindVisible(by).sendKeys(string);
    }

    public void clickTo(By by) {
        waitForFindClick(by).click();
    }

    public void hoverTo(By by) {
        WebElement webElement = webDriver.findElement(by);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(webElement).perform();
    }

    public void runScriptTo(By by) {
        WebElement webElement = waitForFindClick(by);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)webDriver;
        javascriptExecutor.executeScript("arguments[0].click();", webElement);
    }

    public List<WebElement> findElements(By by) {
        waitForFindVisible(by);
        return webDriver.findElements(by);
    }

    private WebElement waitForFindVisible(By by) {
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
        return webDriver.findElement(by);
    }

    private WebElement waitForFindClick(By by) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(by));
        return webDriver.findElement(by);
    }

    public static void scrollToBottom(WebDriver driver) {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public void scrollTo(By by) {
        WebElement webElement = waitForFindVisible(by);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView();", webElement);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(webElement);
        actions.perform();
    }
}
