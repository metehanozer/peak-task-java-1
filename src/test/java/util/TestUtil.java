package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import page.LoginPage;
import page.OpDefinitionPage;

public abstract class TestUtil {

    String baseurl = DataManager.getInstance().getBaseUrl();
    public String username = DataManager.getInstance().getUsername();
    public String password = DataManager.getInstance().getPassword();

    public WebDriver webDriver;
    protected LoginPage loginPage;
    protected OpDefinitionPage opDefinitionPage;

    // browser PARAMETRESİ VERİLMEZSE DEFAULT OLARAK CHROME KULLANILIR.
    @BeforeClass
    @Parameters({"browser"})
    public void setupDriver(@Optional("Chrome") String browser) throws MalformedURLException {

        if (browser.equalsIgnoreCase("chrome")) {
            //ChromeOptions chromeOptions = new ChromeOptions();
            //chromeOptions.addArguments("--incognito");
            System.setProperty("webdriver.chrome.driver", System.getenv("CHROME_DRIVER"));
            //webDriver = new ChromeDriver(chromeOptions);
            webDriver = new ChromeDriver();
            webDriver.manage().window().maximize();

        } else if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", System.getenv("GECKO_DRIVER"));
            webDriver = new FirefoxDriver();
            webDriver.manage().window().maximize();

        } else {
            System.out.println(browser + " tarayıcı desteklenmiyor!");
            System.exit(1);
        }
        webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

//    @BeforeMethod(alwaysRun = true)
//    public void setupLoginPage() {
//        loginPage = new LoginPage(webDriver, baseurl);
//    }

    @BeforeMethod(alwaysRun = true)
    public void loginApp() {
        loginPage = new LoginPage(webDriver, baseurl);
        loginPage.login(username, password);
        opDefinitionPage = new OpDefinitionPage(webDriver);
        opDefinitionPage.verifyLoginUser(username);
    }

    @AfterMethod(alwaysRun = true)
    public void logoutApp() {
        loginPage.logout();
    }

    @AfterMethod
    public void deleteNewOperator() {
        // EKLENEN OPERATOR VERİ TABANINDAN SİLİNEBİLİR.
    }

    @AfterClass
    public void quitDriver() {
        webDriver.quit();
    }

}
