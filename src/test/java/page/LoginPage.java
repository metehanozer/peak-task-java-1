package page;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    //private final By usernameLabel = By.cssSelector("#userNamePlaceHolder");
    @FindBy(css = "#userNamePlaceHolder")
    private WebElement usernameLabel;

    //private final By passwordLabel = By.cssSelector("#passwordPlaceHolder");
    @FindBy(css = "#passwordPlaceHolder")
    private WebElement passwordLabel;

    //private final By loginLabel = By.cssSelector("#loginLabel");
    @FindBy(css = "#loginLabel")
    private WebElement loginLabel;

    @FindBy(css = ".kt-header__topbar-user")
    private WebElement topBarUser;

    @FindBy(css = "user-account-management pm-button > dx-button[role='button'] > .dx-button-content.dx-template-wrapper")
    private WebElement signOutButton;

    public LoginPage(WebDriver webDriver, String baseurl) {
        webDriver.get(baseurl);
        // LOGIN SAYFASINDA OTOMATİK GİRİŞ BİLGİLERİ GELDİĞİ İÇİN BURAYA SLEEP KOYMAK ZORUNDA KALDIM.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        PageFactory.initElements(webDriver, this);
    }

    @Step("Login the application by using credentials...")
    public void login(String username, String password) {
        usernameLabel.clear();
        usernameLabel.sendKeys(username);

        passwordLabel.clear();
        passwordLabel.sendKeys(password);

        loginLabel.click();
    }

    @Step("Logout the application...")
    public void logout() {
        topBarUser.click();
        signOutButton.click();
    }
}
