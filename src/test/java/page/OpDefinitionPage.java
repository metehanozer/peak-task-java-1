package page;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class OpDefinitionPage {

    @FindBy(css = "#okButton [role]")
    private WebElement popupButton;

    @FindBy(css = ".kt-margin-0.kt-margin-r-5")
    private WebElement topbarUser;

    @FindBy(css = "#newButton [role]")
    private WebElement newButton;

    //@FindBy(css = ".ng-pristine.dx-show-clear-button .dx-texteditor-input")
    @FindBy(css = "#code .dx-texteditor-input")
    private WebElement codeInput;

    @FindBy(css = "[aria-haspopup='listbox']")
    private WebElement polyvalenceCombo;

    @FindBy(css = "div:nth-of-type(2) > .dx-item-content.dx-list-item-content")
    private WebElement polyvalenceComboItem;

    @FindBy(css = "#name .dx-texteditor-input")
    private WebElement nameSurnameInput;

    @FindBy(css = "#password .dx-texteditor-input")
    private WebElement passwordInput;

    @FindBy(css = "#startDate .dx-dropdowneditor-icon")
    private WebElement startDateIcon;

    @FindBy(css = ".dx-toolbar-before > .dx-item.dx-toolbar-button.dx-toolbar-item > .dx-item-content.dx-toolbar-item-content > div[role='button']")
    private WebElement todayButton;

    @FindBy(css = ".dx-datebox-wrapper.dx-datebox-wrapper-calendar.dx-datebox-wrapper-datetime.dx-popup-cancel-visible.dx-popup-done-visible.dx-popup-wrapper  div[role='toolbar']  .dx-toolbar-after > div:nth-of-type(1) > .dx-item-content.dx-toolbar-item-content > div[role='button']")
    private WebElement okButton;

    @FindBy(css = "#saveButton > dx-button")
    private WebElement saveOpButton;

    @FindBy(xpath = "//button[contains(text(),'Yes')]")
    private WebElement areUSureButton;

    @FindBy(css = "div#swal2-content")
    private WebElement successDiv;

    @FindBy(xpath = "//button[contains(text(),'OK')]")
    private WebElement successDivClose;

    @FindBy(css = "td:nth-of-type(2) input[role='textbox']")
    private WebElement opCodeFilter;

    //@FindBy(css = "[role]:nth-of-type(6) [aria-describedby='dx-col-14']")
    @FindBy(css = "div:nth-of-type(6) table[role='presentation']  tr[role='row'] > td:nth-of-type(2)")
    private WebElement opCodeSectionInFilter;

    public OpDefinitionPage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

    @Step("Verify that welcome message is 'Hi username' at the top right of the screen...")
    public void verifyLoginUser(String username) {
        // POPUP AÇILMASI İÇİN 2 SANİYE BEKLENİYOR.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        popupButton.click();
        Assert.assertEquals(
                topbarUser.getText(),
                "Hi " + username,
                "Kullanıcı girişi hatası!");
    }

    @Step("Press 'New' Button to add new Operator item...")
    public void addNewOp() {
        newButton.click();
    }

    @Step("Enter the information and press 'Save' button...")
    public void addNewOpInfo(String opCode, String polyvalence, String opUser, String opPass) {
        codeInput.sendKeys(opCode);
        polyvalenceCombo.click();
        // TODO polyvalence DEĞERİNE GÖRE SEÇİM YAPILACAK...
        //Select select = new Select(polyvalenceCombo);
        //select.selectByIndex(Integer.parseInt(polyvalence));
        polyvalenceComboItem.click();
        nameSurnameInput.sendKeys(opUser);
        passwordInput.sendKeys(opPass);
        startDateIcon.click();
        todayButton.click();
        okButton.click();
        saveOpButton.click();
    }

    @Step("Press 'Yes' button to save the data...")
    public void confirmNewOpCode() {
        areUSureButton.click();
    }

    @Step("Verify that success message contains 'Was recorded!' then press 'OK' button...")
    public void verifyAddingNewOperatorMessage() throws InterruptedException {
        // BİR ÖNCEKİ POPUP KAPANIP YENİSİ AÇILANA KADAR 2 SANİYE BEKLENİYOR.
        Thread.sleep(2000);
        Assert.assertEquals(
                successDiv.getText(),
                "The registration process has been successfully realized.",
                "Operatör ekleme hatası!");
        successDivClose.click();
    }

    @Step("Filter by 'Code' column with operator code and verify that...")
    public void verifyNewOperatorCode(String opCode) throws InterruptedException {
        opCodeFilter.sendKeys(opCode);
        // FİLTRENİN ÇALIŞMASI İÇİN 2 SANİYE BEKLENİYOR.
        Thread.sleep(2000);
        Assert.assertEquals(
                opCodeSectionInFilter.getText(),
                opCode,
                "Eklenen operatör bulunamadı!");
    }
}
